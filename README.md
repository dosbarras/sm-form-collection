smFormCollectionPlugin
======================

Plugin que permite administrar collecciones de formularios a partir de una
relación establecida (composición).

Instalación
-----------

Vía composer:

    composer require dosbarras/sm-form-collection:dev-master

Habilitar el plugin en ProjectConfiguration, pero antes establecer la ubicación del mismo:

    $this->setPluginPath('smFormCollectionPlugin', dirname(__FILE__).'/../vendor/dosbarras/sm-form-collection/smFormCollectionPlugin');

    $this->enablePlugins(..., 'smFormCollectionPlugin');

Limpiar la caché:

    rm -rf cache/*

Publicar los assets:

    php symfony plugin:publish-assets

Cambiar el base form de doctrine (BaseFormDoctrine.class.php) por el 
suministrado por el plugin:

    abstract class BaseFormDoctrine extends smFormCollection

El plugin incluye automáticamente un javascript el cual depende de 
jQuery. Queda como tarea del programador proporcionarlo.

Uso
---

Suponiendo el siguiente modelo:

    Persona:
      columns:
        nombre: { type: string(45), notnull: true }

    PersonaDomicilio:
      columns:
        persona_id: { type: integer, notnull: true }
        tipo: { type: enum, values: [Particular, Laboral], notnull: true }
        direccion: { type: string(90), notnull: true }
      relations:
        Persona:
          foreignAlias: Domicilios

Configuración necesaria en PersonaForm:

    public function configure()
    {
      $this->buildCollection('Domicilios as domicilio');
    }    

Configuración necesaria en PersonaDomicilioForm:

    public function configure()
    {
      unset($this['persona_id']);
    }

Renderizando el formulario PersonaForm en forma manual:

    <?php use_javascripts_for_form($form) ?>
    <?php use_stylesheets_for_form($form) ?>

    <?php echo form_tag_for($form, 'persona/new') ?>
      <?php echo $form->renderHiddenFields(false) ?>
      <table>

        <?php echo $form['nombre']->renderRow() ?>

        <tr>
          <th><?php echo $form['domicilios']->renderLabel() ?></th>
          <td>

            <table data-collection-container="domicilios">
              <?php foreach ($form['domicilios'] as $name => $domicilioForm): ?>
                <?php if ($name == '__prototype__') continue ?>
                <?php render_domicilio_form($domicilioForm) ?>
              <?php endforeach ?>
            </table>

            <button type="button" data-collection-add="domicilios">Agregar</button>

          </td>
        </tr>

        <tr>
          <td colspan="2">
            <button type="submit">Guardar</button>
          </td>
        </tr>

      </table>
    </form>

    <?php // collection: domicilio ?>

    <?php function render_domicilio_form($form) { ?>
      <div data-collection-item>
        <?php echo $form->renderHiddenFields(false) ?>
        <?php echo $form['tipo']->renderRow() ?>
        <?php echo $form['direccion']->renderRow() ?>
        <tr>
          <th>&nbsp;</th>
          <td><button type="button" data-collection-remove>Quitar</button></td>
        </tr>
      </div>
    <?php } ?>

    <script type="text/html" data-collection-prototype="domicilios">
      <?php render_domicilio_form($form['domicilios']['__prototype__']) ?>
    </script>

    <?php // fin collection: domicilio ?>

### Usándolo en el admin-generator con el theme tb3

El theme tb3 autodetectará la colección. Pero además es posible configurar un par de 
ítems:

    $this->buildCollection('Domicilios as domicilio', array(
      'allowAdd'    => true,
      'allowDelete' => true,
    ));

En la documentación del método buildCollection se especifican con más detalles
el resto de configuraciones disponibles.