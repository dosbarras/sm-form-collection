(function($){
$(function(){

  $('[data-collection-container]').each(function(){
    
    var $container = $(this);
    var collectionName = $container.attr('data-collection-container');

    $container.data('next-index', parseInt($container.find('[data-collection-item]').length));
    
    var prototype = $('[data-collection-prototype="'+collectionName+'"]').html();
    
    $(window.document).on('click', '[data-collection-add="'+collectionName+'"]', function(e){
      e.preventDefault();
      $container.append(prototype.replace(/__prototype__/g, $container.data('next-index')));
      $container.data('next-index', $container.data('next-index') + 1);
    });
    
    $(window.document).on('click', '[data-collection-remove]', function(e){
      e.preventDefault();
      $(this).parents('[data-collection-item]').remove();
    });
    
  });    
    
});
})(jQuery);