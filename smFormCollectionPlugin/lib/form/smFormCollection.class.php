<?php

abstract class smFormCollection extends sfFormDoctrine
{
  protected $collections = array();
  
  /**
   * Crear una colección a partir de una relación, ej:
   * 
   *     [php]
   *     $form = new UserForm();
   *     $form->buildCollection('Groups as groups');
   * 
   * Las opciones de configuración son las siguientes (todas opcionales):
   * 
   *  * formClass:    clase del formulario que se utilizará en la coleción
   *  * formArgs:     argumentos que se pasarán al instanciar formClass
   *  * newFormClass: clase del formulario para nuevos elementos (prototype)
   *  * newFormArgs:  argumentos que se pasarán al instanciar newFormClass
   *  * newObject:    objeto que se pasará como nuevo al newFormClass
   *  * allowAdd:     indica si debe permitir agregar más elementos
   *  * allowDelete:  indica si debe permitir remover elementos
   * 
   * @param string $relationName
   * @param array  $config
   */
  public function buildCollection($relationName, $config = array())
  {
    // se determina el nombre del campo y el nombre de la relación
    if (false !== $pos = stripos($relationName, ' as '))
    {
      $fieldName = substr($relationName, $pos + 4);
      $relationName = substr($relationName, 0, $pos);
    }
    else
    {
      $fieldName = $relationName;
    }
    
    // creo relación y establezco la configuración inicial
    $this->collections[$fieldName] = array_merge(array(
        'formClass'    => null,
        'formArgs'     => array(),
        'newFormClass' => null,
        'newFormArgs'  => array(),
        'newObject'    => null,
        'allowDelete'  => true,
        'allowAdd'     => true,
    ), $config);
      
    $relation = $this->getObject()->getTable()->getRelation($relationName);

    $formClass = null === $this->collections[$fieldName]['formClass'] ? 
            $relation->getClass().'Form' : $this->collections[$fieldName]['formClass'];
    
    $reflectionForm = new ReflectionClass($formClass);

    $collectionForm = new smFormCollectionContainer();
    $collectionForm->getWidgetSchema()->setAttribute('allow_delete', $this->collections[$fieldName]['allowDelete']);
    $collectionForm->getWidgetSchema()->setAttribute('allow_add', $this->collections[$fieldName]['allowAdd']);

    // se embebe un form por cada objeto de la relación
    foreach ($this->getObject()->$relationName as $index => $childObject)
    {
      $form = $reflectionForm->newInstanceArgs(array_merge(
              array($childObject), 
              $this->collections[$fieldName]['formArgs']
      ));
      $collectionForm->embedForm($index, $form);
      $collectionForm->getWidgetSchema()->setLabel($index, (string) $childObject);
    }

    // se crea el objeto para el form prototipo
    if (null === $this->collections[$fieldName]['newObject'])
    {
      $childObjectClass = $relation->getClass();
      $newChildObject = new $childObjectClass();
    }
    else
    {
      $newChildObject = $this->collections[$fieldName]['newObject'];
    }
    
    // creo y embebo el form proptotipo
    $newFormClass = null === $this->collections[$fieldName]['newFormClass'] ? 
            $relation->getClass().'Form' : $this->collections[$fieldName]['newFormClass'];
    
    $reflectionNewForm = new ReflectionClass($newFormClass);
    
    $form = $reflectionNewForm->newInstanceArgs(array_merge(
            array($newChildObject), 
            $this->collections[$fieldName]['newFormArgs']
    ));
      
    $form->setValidatorSchema(new smValidatorSchemaPass());
    
    $collectionForm->embedForm('__prototype__', $form);
    
    // embebo la colección al formulario principal (manualmente)
    $this->embeddedForms[$fieldName] = $collectionForm;

    $collectionForm = clone $collectionForm;
    unset($collectionForm[self::$CSRFFieldName]);

    $this->setDefault($fieldName, $collectionForm->getDefaults());

    $this->widgetSchema[$fieldName] = $collectionForm->getWidgetSchema();
    $this->validatorSchema[$fieldName] = $collectionForm->getValidatorSchema();

    $this->resetFormFields();    
    #$this->embedForm($fieldName, $collectionForm);
    
    // agrego a la configuraciones algunos valores reutilizables en el resto del proceso
    $this->collections[$fieldName] = array_merge($this->collections[$fieldName], array(
        'collectionForm'    => clone $collectionForm,
        'relationName'      => $relationName,
        'relation'          => $relation,
        'reflectionForm'    => $reflectionForm,
        'reflectionNewForm' => $reflectionNewForm,
        'newChildObject'    => $newChildObject,
    ));
  }
  
  protected function doBind(array $values)
  { 
    foreach ($this->collections as $fieldName => $config)
    {
      if (isset($values[$fieldName]))
      {        
        // quito los formularios de los elementos que no han sido enviado
        foreach ($config['collectionForm'] as $index => $subForm)
        {
          if (!$subForm instanceof sfFormFieldSchema)
          {
            continue;
          }
         
          if (!isset($values[$fieldName][$index]) && $index !== '__prototype__')
          {            
            unset($this->embeddedForms[$fieldName][$index]);
            unset($this->widgetSchema[$fieldName][$index]);
            unset($this->validatorSchema[$fieldName][$index]);
          }
        }
        
        // agrego los nuevos formularios para los nuevos elementos enviados
        foreach ($values[$fieldName] as $index => $subValues)
        {          
          if (!isset($this->embeddedForms[$fieldName][$index]))
          { 
            $newObject = clone $config['newChildObject'];
            $this->getObject()->{$config['relationName']}[] = $newObject;
            
            $form = $config['reflectionNewForm']->newInstanceArgs(array_merge(
                    array($newObject), 
                    $config['newFormArgs']
            ));

            // hago un embed manual (sacado del código original embedForm)
            $this->embeddedForms[$fieldName]->embedForm($index, $form);

            $form = clone $form;
            unset($form[self::$CSRFFieldName]);

            $widgetSchema = $form->getWidgetSchema();

            $this->embeddedForms[$fieldName]->setDefault($index, $form->getDefaults());

            $decorator = $widgetSchema->getFormFormatter()->getDecoratorFormat();

            $this->widgetSchema[$fieldName][$index] = new sfWidgetFormSchemaDecorator($widgetSchema, $decorator);
            $this->validatorSchema[$fieldName][$index] = $form->getValidatorSchema();

            $this->embeddedForms[$fieldName]->resetFormFields();
          }
        }
      }
      else
      {
        // si no se envió ningun elemento de la relación elimino todos los forms
        if (isset($this->embeddedForms[$fieldName]))
        {
          foreach ($this->embeddedForms[$fieldName] as $index => $form)
          {
            if ($index == '__prototype__')
            {
              continue;
            }
            
            unset($this->embeddedForms[$fieldName][$index]);
            unset($this->widgetSchema[$fieldName][$index]);
            unset($this->validatorSchema[$fieldName][$index]);               
          }
        }
      }
    }
    
    parent::doBind($values);
  }
  
  public function saveEmbeddedForms($con = null, $forms = null, $parentName = null)
  { 
    if (null === $con)
    {
      $con = $this->getConnection();
    }

    if (null === $forms)
    {
      $forms = $this->embeddedForms;
    }

    $saved = array();
    
    foreach ($forms as $index => $form)
    {
      if ($form instanceof sfFormObject && $index !== '__prototype__')
      {         
        $form->getObject()->save($con);
        $saved[$index] = true;
        $form->saveEmbeddedForms($con);
      }
      else
      {
        $this->saveEmbeddedForms($con, $form->getEmbeddedForms(), $index);
      }
    }
    
    if ($parentName && isset($this->collections[$parentName]))
    { 
      foreach ($this->collections[$parentName]['collectionForm'] as $index => $form)
      { 
        if (!$form instanceof sfFormFieldSchema)
        {
          continue;
        }
        
        if ($form instanceof sfFormFieldSchema && !isset($saved[$index]))
        { 
          $this->collections[$parentName]['collectionForm']->getEmbeddedForm($index)->getObject()->delete();
        }
      }
    }
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    
    foreach ($this->collections as $fieldName => $config)
    { 
      if ($fieldName !== $config['relationName'] && isset($values[$fieldName]))
      {
        $values[$config['relationName']] = $values[$fieldName];
        unset($values[$fieldName]);
      }      
      
      unset($values[$config['relationName']]['__prototype__']);
    }
    
    return $values;
  }  
  
  public function getJavaScripts() 
  { 
    return array_merge(parent::getJavaScripts(), array('/smFormCollectionPlugin/smFormCollection.js'));
  }
}

