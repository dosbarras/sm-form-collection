<?php

class smFormCollectionContainer extends sfForm
{
  public function __construct($defaults = array(), $options = array(), $CSRFSecret = null)
  {
    parent::__construct($defaults, $options, $CSRFSecret);
    
    $this->widgetSchema    = new smWidgetFormSchemaCollection();
  }  
}
